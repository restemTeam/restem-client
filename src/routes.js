import Home from './pages/index.vue';
import Login from './pages/login.vue';
import Category from './pages/category.vue';
import NotFoundPage from './pages/not-found.vue';

export default [{
        path: '/',
        component: Home
    },
    {
        path: '/category/demo/',
        component: Category
    },
    {
        path: '/login/',
        component: Login
    },
    {
        path: '(.*)',
        component: NotFoundPage
    }
];